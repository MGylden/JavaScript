#Assignment 4 - Komputer Store

Takes laptops from komputer api, and shows image/specs/description accordingly to laptop chosen

1. A user is able to work for 100 kr.
2. a user is able to get a loan for a max of 2*bankbalance.
3. a user is able to pay loan, max of 1 loan at a time.
4. a user is able to transfer salary to bank.
5. a user is able to pay loan - if user has more in salary rest will be payed to bank -- button is hidden if no loan has been taken.
6. a user is able to buy laptops for the amount available in bank balance.

Written by: Mathias Gylden.
